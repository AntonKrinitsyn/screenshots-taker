# Screenshots-taker
---
## Content
1. [ Description. ](#desc)
2. [ Usage tips. ](#usage)
<a name="desc"></a>
## Requirenments
- **aws** credentials should be configured
- run `yarn install` in project directory
- provide environments variables. Sample can be found in _.env.sample_
## Commands
- `yarn dev` to setup **dev** env
- `yarn start` to start server in **prod** mode
- `yarn test`: to start **one** test iteration
- `yarn test-watch`: to run test in **watch** mode
- `yarn check`: to start _eslint_ check
- `yarn check-watch`: to run _eslint_ check in **watch** mode
## Docker
- `docker build -t screenshots-taker:latest .` to build image
- `docker run --rm -p "4000:4000" -it screenshots-taker:latest` to run container
## Docker-Compose
  **Yarn** commands could be used for  _testing_, _eslinting_, _developing_ in containers to keep your environment clean. Add `docker-compose run --rm screenshots-taker` before any yarn command. Ex. here is equivalent of `yarn test-watch` running via _docker-compose_:
  ```bash
  docker-compose run --rm screenshots-taker yarn test-watch
  ``` 
